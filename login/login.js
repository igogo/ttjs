
if (Meteor.isClient) {
    Template.login_form.events({
        'submit #login-form' : function(e, t){
            e.preventDefault();
            var email = t.find('#login-email').value,
                password = t.find('#login-password').value;

            email = trimInput(email);

            Meteor.loginWithPassword(email, password, function(err){
                if (err) {
                    console.error(err);
                    Session.set('displayMessage', 'Ошибка &amp; Не верный логин или пароль.');
                } else {
                    console.log('YO!');
                }
            });

            return false;
        }
    });

    Template.register_form.events({
        'submit #register-form' : function(e, t) {
            e.preventDefault();
            var email = t.find('#register-email').value,
                password = t.find('#register-password').value;

            email = trimInput(email);
            if (isValidPassword(password)) {

                Accounts.createUser({
                    email: email,
                    username: email.split("@")[0],
                    password : password
                }, function(err){
                    if (err) {
                        console.error(err);
                        Session.set('displayMessage', 'Ошибка &amp; Нужно указать email.');
                    } else {
                        console.log('YO!');
                    }
                });
            }

            return false;
        }
    });
}

if (Meteor.isServer) {

    console.log('Accounts', Accounts);
    Accounts.onCreateUser(function(options, user){
        var profile = {
            location: locations[0]
        };
        
        user.profile = profile;

        return user;
    });

}
