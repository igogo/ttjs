if (Meteor.isClient) {
    Template.input.events({
        'click .sendMsg': function(e) {
           _sendMessage();
        },
        'keyup #msg': function(e) {
          if (e.type == "keyup" && e.which == 13) {
            _sendMessage();
          }
        }
    });

  _sendMessage = function() {
    var el = document.getElementById("msg");
    Messages.insert({
        user: Meteor.user().username,
        msg: el.value,
        ts: new Date(),
        uid: Meteor.userId()
    });
    
    el.value = "";
    el.focus();
  };
}
