if (Meteor.isClient) {
    Handlebars.registerHelper('selected', function(foo) {
        console.log(foo == Meteor.user().profile.location, [foo, Meteor.user().profile.location]);
      return foo == Meteor.user().profile.location;
    });

    Template.config.events({
        'submit #config-form' : function(e, t){
            e.preventDefault();
            var email = t.find('#email').value,
                old_password = t.find('#old_password').value,
                password = t.find('#password').value,
                location = t.find('#location').value,
                old_location = Meteor.user().profile.location;

            email = trimInput(email);

            if (old_password != '' && password != '' && isValidPassword(password)) {
                Accounts.changePassword(old_password, password, function(err, data){
                    console.log(err, data);

                    if (err) {
                        Notifications.error('Ошибка', 'Не корpектный пароль!');
                    } else {
                        Notifications.success('Сохранние!', 'Вы успешно сменили пароль!');
                    }
                });

            }

            Meteor.users.update({'_id': Meteor.userId()}, {$set : {
                emails: [{address: email}],
                username: email.split("@")[0],
                "profile.location": location
            }}, function(err){
                if (err) {
                    console.error(err);
                    Session.set('displayMessage', 'Ошибка &amp; не возможно изменить данные пользователя.');
                } else {
                    Notifications.success('Сохранние!', 'Вы успешно сменили данные пользователя!');
                }
            });

            return false;
        }
    });
}
