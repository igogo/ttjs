Users = Meteor.users;
Messages = new Meteor.Collection("messages");
Rooms = new Meteor.Collection("rooms");

Users.allow({
   update: function(userId, doc) {
       console.log('doc.userId === userId', [doc._id, userId]);
       return doc && doc._id === userId;
   }
});
