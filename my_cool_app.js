trimInput = function(val) {
    return val.replace(/^\s*|\s*$/g, "");
};

isValidPassword = function(val, field) {
   if (val.length >= 6) {
     return true;
   } else {
     Session.set('displayMessage', 'Ошибка &amp; Слишком простой пароль.');
     return false;
   }
};

locations = ['location 1', 'location 2', 'location 3'];

if (Meteor.isClient) {

    Meteor.autorun(function() {
      // Whenever this session variable changes, run this function.
      var message = Session.get('displayMessage');
      if (message) {
        var stringArray = message.split('&amp;');
        Notifications.error(stringArray[0], stringArray[1]);
        Session.set('displayMessage', null);
      }
    });

    Meteor.subscribe("rooms");
    Meteor.subscribe("messages");
    Session.setDefault("roomname", function() {
        return Meteor.user() && Meteor.user().profile.location ? Meteor.user().profile.location : locations[0];
    });

    Router.configure({
      layoutTemplate: 'ApplicationLayout',
      'data': {
          isLogined: function () {
              return Meteor.userId();
          },
          username: function () {
              return Meteor.user() ? Meteor.user().username : '';
          }
      }
    });

    Router.onBeforeAction(function() {
        if (! Meteor.userId()) {
            this.render('login');
        } else {
            this.next();
        }
    });

    Router.route('/logout', function () {
        Meteor.logout();
        this.redirect('/');
    });

    Router.route('/config', function () {
        this.render('config', {
            data: {
                locations: locations,
                email: Meteor.user().emails[0].address
            }
        });
    });

    Router.route('/', function () {
        this.render('main', {
            data: {
                roomname: function () {
                    return Meteor.user().profile.location;
                },
                messages: function () {
                    var uids = Users.find({"profile.location": Meteor.user().profile.location}, {fields: {_id: 1}}).map(function(user) {
                        return user._id;
                    });

                    return Messages.find({uid: {$in: uids}});
                }
            }
        });
    });

}


if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup

    Rooms.remove({});
    if (Rooms.find().count() === 0) {
      locations.forEach(function(r) {
        Rooms.insert({roomname: r});
      });
    }
  });
}
